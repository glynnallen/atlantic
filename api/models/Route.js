/**
 * Route.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // e.g., "Glynn"
    name: {
      type: 'string'
    },
    distance: {
      type: 'integer'
    },
    // e.g., "Glynn"
    elevation: {
      type: 'integer'
    },
    difficultyRating: {
      type: 'integer'
    },
    events: {
      collection: 'event',
      via: 'route'
    }
  }
};

