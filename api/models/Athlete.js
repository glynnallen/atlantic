/**
 * Athlete.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  // An athlete may only belong to a single club
  attributes: {
    // e.g., "Glynn"
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    mobileNumber: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    age: {
      type: 'number'
    },
    // Associations (aka relational attributes)
    sibling: { model: 'Athlete' },
    spouse: { model: 'Athlete' },
    // Attribute methods
    fullName: function (){
      return this.firstName + ' ' + this.lastName;
    },
    isEligibleForDiscount: function (){
      return this.age >= 65;
    },
    // Add a reference to event
    events: {
      collection: 'event',
      via: 'participants',
      dominant: true
    },
    membership: {
      model: 'club'
    }
  }
};

