/**
 * Event.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    eventDate: {
      type: 'date'
    },
    number: {
      type: 'integer',
      autoIncrement: true
    },
    participants: {
      collection: 'athlete',
      via: 'events'
    },
    route: {
      model: 'route'
    }
  }
};

