/**
 * Club.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // e.g., "RAC"
    name: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    // Add a reference to Pets
    members: {
      collection: 'athlete',
      via: 'membership'
    }
  }
};

